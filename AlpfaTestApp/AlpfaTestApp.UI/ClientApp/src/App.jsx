import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Popular from './components/Popular';
import Top from "./components/Top";
import Actual from "./components/Actual";
import MyCallbackComponent from "./components/CallbackComponent";

export default () => (
  <Layout>
    <Route exact path='/' component={Home} />

  
        <Route path='/popular' component={Popular} />
        <Route path='/actual' component={Actual} />
        <Route path='/top' component={Top} />
        <Route path='/callback' component={MyCallbackComponent} />

  </Layout>
);
