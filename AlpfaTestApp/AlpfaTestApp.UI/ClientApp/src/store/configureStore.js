import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { createUserManager, loadUser, reducer } from 'redux-oidc';
import * as RoleSelector from "./RoleSelectorReducer";
import * as PopularContent from "./PopularContentReducer";
import * as TopContent from "./TopContentReducer";
import * as ActualContent from "./ActualContentReducer";
import * as MenuReducer from "./MenuReducer";
import * as HomeReducer from "./HomeReducer";


var config = {
    authority: "http://localhost:5015",
    client_id: "js",
    redirect_uri: "http://localhost:5003/callback",
    response_type: "id_token token",
    scope: "openid profile api1",
    post_logout_redirect_uri: "http://localhost:5003",
    silent_redirect_uri: "http://localhost:5003/callback",
    
    automaticSilentRenew: true,
    filterProtocolClaims: true,
    loadUserInfo: true
};
export const userManager = createUserManager(config);
//const oidcMiddleware = createOidcMiddleware(userManager, () => true, false, '/callback');

export default function configureStore(history, initialState) {
    const reducers = {
      
        roleSelector: RoleSelector.reducer,
        popularContent: PopularContent.reducer,
        topContent: TopContent.reducer,
        actualContent: ActualContent.reducer,
        menu: MenuReducer.reducer,
        home: HomeReducer.reducer, 
        oidc: reducer
    };

    const loggerMiddleware = store => next => action => {
        console.log("Action type:", action.type);
        console.log("Action payload:", action.payload);
        console.log("State before:", store.getState());
        next(action);
        console.log("State after:", store.getState());
    };
    


    const middleware = [
        thunk,
        routerMiddleware(history),
        loggerMiddleware
        //oidcMiddleware
    ];

    // In development, use the browser's Redux dev tools extension if installed
    const enhancers = [];
    const isDevelopment = process.env.NODE_ENV === 'development';
    if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
        enhancers.push(window.devToolsExtension());
    }

    const rootReducer = combineReducers({
        ...reducers,
        routing: routerReducer
    });

    var store = createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware), ...enhancers)
    );
    loadUser(store, userManager);

    return store;
}
