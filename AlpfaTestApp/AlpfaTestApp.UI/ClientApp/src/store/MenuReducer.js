import { userManager } from "./configureStore";


const changeAnalyticRoleType = 'CHANGE_ANALYTIC_ROLE';
const changeManagerRoleType = 'CHANGE_MANAGER_ROLE';
const changeChiefRoleType = 'CHANGE_CHIEF_ROLE';
const initMenuType = 'INIT_MAIN_MENU';
const initialState = { m_isManager: false, m_isChief: false, m_isAnalytic:false };
const changeRoleInitType = 'CHANGE_ROLE_INIT';
export const actionCreators = {
   
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === initMenuType) {
       
        return initialState;
    }

    if (action.type === changeAnalyticRoleType) {
        return { ...state, m_isAnalytic: !state.m_isAnalytic };
    }

    if (action.type === changeManagerRoleType) {
        return { ...state, m_isManager: !state.m_isManager };
    }
    if (action.type === changeChiefRoleType) {
        return { ...state, m_isChief: !state.m_isChief };
    }
    if (action.type === changeRoleInitType) {

        return { ...state,
            m_isChief: action.status.chief,
            m_isManager: action.status.manager,
            m_isAnalytic: action.status.analytic
        };
    }
    return state;
};
