import axios from 'axios';
import { userManager } from "./configureStore";

const changeAnalyticRoleType = 'CHANGE_ANALYTIC_ROLE';
const changeManagerRoleType = 'CHANGE_MANAGER_ROLE';
const changeChiefRoleType = 'CHANGE_CHIEF_ROLE';
const changeRoleInitType = 'CHANGE_ROLE_INIT';


const initialState = { isChief: false, isManager: false, isAnalytic: false };

export const actionCreators = {
    changeAnalyticRole: () => async (dispatch, getState) => {
        userManager.getUser().then( async  function(user) {
            if (user) {
                var config = {
                    headers: { 'Authorization': "bearer " + user.access_token }

                };
                await axios.put('https://localhost:5001/api/roleManager/setAnalyticRole',
                    { newValue: !getState().roleSelector.isAnalytic },
                        config)
                    .then(res => {
                        const prod = res.data;
                        if (prod) {
                            userManager.signinSilent();
                            dispatch({ type: changeAnalyticRoleType });
                        } else {
                            alert("Не удалось изменить роль");
                        }
                    }).catch(error => {
                        if (error.response.status === 401) {
                            alert("Не авторизован, выполните вход");
                        }
                        if (error.response.status === 403) {
                            alert("Нет доступа, обновите страницу или выполните вход");
                        }
                    });
            } else {
                alert("User not logged in");
            }
        });

    },
    changeManagerRole: () => async (dispatch, getState) => {
        userManager.getUser().then(async  function(user) {
            if (user) {
                var config = {
                    headers: { 'Authorization': "bearer " + user.access_token }

                };
                await axios.put('https://localhost:5001/api/roleManager/setManagerRole',
                    { newValue: !getState().roleSelector.isManager },
                        config)
                    .then(res => {
                        const prod = res.data;
                        if (prod) {
                            userManager.signinSilent();
                            dispatch({ type: changeManagerRoleType });
                        } else {
                            alert("Не удалось изменить роль");
                        }
                    }).catch(error => {
                        if (error.response.status === 401) {
                            alert("Не авторизован, выполните вход");
                        }
                        if (error.response.status === 403) {
                            alert("Нет доступа, обновите страницу или выполните вход");
                        }
                    });
            } else {
                alert("User not logged in");
            }
        });

    },
    changeChiefRole: () => async (dispatch, getState) => {
        userManager.getUser().then(async  function(user) {
            if (user) {
                var config = {
                    headers: { 'Authorization': "bearer " + user.access_token }

                };
               await  axios.put('https://localhost:5001/api/roleManager/setChiefRole',
                    { newValue: !getState().roleSelector.isChief },
                        config)
                    .then(res => {
                        const prod = res.data;
                        if (prod) {
                            userManager.signinSilent();
                            dispatch({ type: changeChiefRoleType });
                        } else {
                            alert("Не удалось изменить роль");
                        }
                   }).catch(error => {
                        if (error.response.status === 401) {
                            alert("Не авторизован, выполните вход");
                        }
                        if (error.response.status === 403) {
                            alert("Нет доступа, обновите страницу или выполните вход");
                        }
                    });
            } else {
                alert("User not logged in");
            }
        });

    },
    initComponent: () => async (dispatch, getState) => {

        userManager.getUser().then(function(user) {
            if (user) {
                var status = { analytic: false, manager: false, chief: false };

                var roles = user.profile["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
                if (Array.isArray(roles)) {
                    roles.forEach(function(role) {
                        if (role === 'Analytic') {
                            status.analytic = true;
                        }
                        if (role === 'Manager') {
                            status.manager = true;
                        }
                        if (role === 'Chief') {
                            status.chief = true;
                        }
                    });
                } else {
                    if (roles === 'Analytic') {
                        status.analytic = true;
                    }
                    if (roles === 'Manager') {
                        status.manager = true;
                    }
                    if (roles === 'Chief') {
                        status.chief = true;
                    }
                }
                dispatch({ type: changeRoleInitType, status: status });
            } else {
                alert("User not logged in");
            }
        });

    }

};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === changeAnalyticRoleType) {
        return { ...state, isAnalytic: !state.isAnalytic };
    }

    if (action.type === changeManagerRoleType) {
        return { ...state, isManager: !state.isManager };
    }
    if (action.type === changeChiefRoleType) {
        return { ...state, isChief: !state.isChief };
    }
    if (action.type === changeRoleInitType) {

        return { ...state,
            isChief: action.status.chief,
            isManager: action.status.manager,
            isAnalytic: action.status.analytic
        };
    }
    return state;
};