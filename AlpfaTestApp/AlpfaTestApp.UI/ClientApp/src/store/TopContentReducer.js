import axios from 'axios';
import { userManager } from "./configureStore";
const requestProductsType = 'REQUEST_TOP_PRODUCT_TYPE';
const recieveProductsType = 'RECIVE_TOP_PRODUCT_TYPE';
const initialState = { isLoading: true , products: []};

export const actionCreators = {
    requestProductsType: () => async (dispatch, getState) => {
        dispatch({ type: requestProductsType });
        userManager.getUser().then(async function (user) {
            if (user) {
                var config = {
                    headers: { 'Authorization': "bearer " + user.access_token }

                }; 
                await axios.get('https://localhost:5001/api/product/top', config)
                    .then(res => {
                        const prod = res.data;
                        dispatch({ type: recieveProductsType, prod });
                    }).catch(error => {
                        if (error.response.status === 401) {
                            alert("Не авторизован, попробуйте еще раз или обновите страницу");
                            userManager.signinSilent();
                        }
                        if (error.response.status === 403) {
                            userManager.signinSilent();
                            alert("Нет доступа, обновите страницу или выполните вход");
                        }
                    });
            }
            else {
                console.log("User not logged in");
            }
        });
       
        
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === requestProductsType) {
        return initialState;
    }

    if (action.type === recieveProductsType) {
        return { ...state, isLoading: false, products: action.prod};
    }
    
    return state;
};
