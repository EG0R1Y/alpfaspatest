import { userManager } from "../store/configureStore";
const initHomeType = 'INIT_HOME';
const initialState = { isLogged: false };


export const actionCreators = {
    requestLogin: () =>  () => {
        userManager.signinRedirect();
    },
    requestLogout: () => () => {
        userManager.signoutRedirect();
    },
    initHome: () => (dispatch, getState) => {
        userManager.getUser().then(function (user) {
            if (user) {
                dispatch({ type: initHomeType, isLog: true });
            }
            else {
                dispatch({ type: initHomeType, isLog: false});
            }
        });
        
    }
};

export const reducer = (state, action) => {
    state = state || initialState;
    if (action.type === initHomeType) {
        return { isLogged: action.isLog}
    }
    return state;
};
