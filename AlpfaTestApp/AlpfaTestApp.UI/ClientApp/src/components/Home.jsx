import React from 'react';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button';
import { bindActionCreators } from 'redux';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import RoleSelectors from "./RoleSelectors"
import { actionCreators } from "../store/HomeReducer";
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'

class Home extends React.Component {

    componentDidMount() {
        this.props.initHome();
    }
    render() {
        return (
            <div>
                <Container>
                    <Row>
                        {this.props.isLogged
                            ? <Col md="auto"><RoleSelectors/></Col>
                            : <Col md="auto">
                                  <h2>Необходимо войти в систему</h2></Col>}

                        <Col xs={1}> </Col>
                        <Col >
                            <div>
                                <ButtonToolbar>
                                    <Button
                                        variant="primary"
                                        size="lg"
                                        onClick={this.props.requestLogin}>
                                        Войти
                                    </Button>
                                    &nbsp; &nbsp; &nbsp; &nbsp;
                                    <Button
                                        variant="primary"
                                        size="lg"
                                       
                                        onClick={this.props.requestLogout}>
                                        Выйти
                                    </Button>
                                </ButtonToolbar>

                            </div></Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default connect(
    state => state.home,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(Home);
