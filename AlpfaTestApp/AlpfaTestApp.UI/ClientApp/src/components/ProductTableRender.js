﻿import Table from 'react-bootstrap/Table'
import React, { Component } from 'react';


export default function renderProductsTable(props) {
    

   
    return (<div>
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>Id товара</th>
                <th>Наименование товара</th>
                <th>Цена</th>
                <th>Количество покупок</th>
            </tr>
            </thead>
            <tbody>
            {props.products.map(product =>
                <tr key={product.id}>
                    <td>{product.id}</td>
                    <td>{product.name}</td>
                    <td>{product.cost}</td>
                    <td>{product.count}</td>

                </tr>
            )}

            </tbody>
        </Table>
    </div>
    );
}