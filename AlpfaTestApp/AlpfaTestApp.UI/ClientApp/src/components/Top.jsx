﻿import React, { Component } from 'react';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import RoleSelectors from "./RoleSelectors"
import TopContent from "./TopContent"

export default class Popular extends Component {


    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col md="auto"><RoleSelectors /></Col>
                        <Col xs={1} > </Col>
                        <Col ><TopContent /></Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

