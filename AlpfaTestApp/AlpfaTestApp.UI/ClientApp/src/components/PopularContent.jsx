﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from "../store/PopularContentReducer";
import  ProductTableRender  from "./ProductTableRender";
import Loader from 'react-loader-spinner'

class PopularContent extends Component {
    componentDidMount() {
        // This method is called when the component is first added to the document
        this.props.requestProductsType();
    }

    render() {
        if (this.props.isLoading) {
            return (
                
                    <Loader
                        type="Watch"
                        color="#00BFFF"
                        height="50"
                        width="50"
                    />  
            );
        } else {
            return (
                <div>
                    <h2>Популярные товары</h2>
                    {renderProductsTable(this.props)}
                </div>
            );
        }
        
    }
}

function renderProductsTable(props) {
    return (
        ProductTableRender(props)
    );
}

export default connect(
    state => state.popularContent,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(PopularContent);
