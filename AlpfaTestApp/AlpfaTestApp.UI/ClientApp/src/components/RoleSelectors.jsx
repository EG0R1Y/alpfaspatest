import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from "../store/RoleSelectorReducer";
import { CustomInput, FormGroup, Label, Form } from 'reactstrap';

class RoleSelector extends Component {
    componentDidMount() {
        // This method is called when the component is first added to the document
        this.props.initComponent();
    }

    render() {
        return (
            <div>
                <Form>
                    <FormGroup>
                    <Label for="exampleCheckbox">Выбор ролей</Label>
                    <div>
                            <CustomInput type="checkbox" id="analyticChangeSwitch" onChange={this.props.changeAnalyticRole} checked={this.props.isAnalytic} name="customSwitch" label="Роль аналитика" />
                            <CustomInput type="checkbox" id="managerChangeSwitch" onChange={this.props.changeManagerRole} checked={this.props.isManager} name="customSwitch" label="Роль менеджера" />
                            <CustomInput type="checkbox" id="chiefChangeSwitch" onChange={this.props.changeChiefRole} checked={this.props.isChief} label="Роль начальника"  />
                    </div>
                    </FormGroup>
                </Form>
        </div>
        );
    }
}


export default connect(
  state => state.roleSelector,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(RoleSelector);
