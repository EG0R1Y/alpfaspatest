import React from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from "../store/MenuReducer";



class NavMenu extends React.Component {
    
    constructor (props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle () {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render () {
        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to="/">AlpfaTestApp.UI</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} className="mr-2"/>
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={this.state.isOpen} navbar>
                            <ul className="navbar-nav flex-grow">
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/">Home</NavLink>
                                </NavItem>
                                <NavItem>
                                    {this.props.m_isAnalytic
                                        ? <NavLink tag={Link} className="text-dark" to="/popular">Популярные</NavLink>
                                        : null}
                                </NavItem>
                                <NavItem>
                                    {this.props.m_isManager
                                        ? <NavLink tag={Link} className="text-dark" to="/actual">Актуальные</NavLink>
                                        : null}
                                </NavItem>
                                <NavItem>
                                    {this.props.m_isChief
                                        ? <NavLink tag={Link} className="text-dark" to="/top">Топ 5</NavLink>
                                        : null}
                                    
                                </NavItem>
                            </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}

export default connect(
    state => state.menu,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(NavMenu);
