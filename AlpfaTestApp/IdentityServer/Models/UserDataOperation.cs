﻿namespace IdentityServer.Models
{
	public enum UserDataOperation
	{
		GetUserByName = 0,
		GetUserById = 1,
		GetUserRoles = 2
	}
}