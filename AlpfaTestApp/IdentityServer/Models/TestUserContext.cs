﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IdentityServer.Models
{
    public partial class TestUserContext : DbContext
    {
        public TestUserContext()
        {
        }

        public TestUserContext(DbContextOptions<TestUserContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<XUserRole> XUserRole { get; set; }
        public virtual DbQuery<ResultRoleChanged> ResultRoleChanged { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=TestUser;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<XUserRole>(entity =>
            {
                entity.ToTable("xUserRole");

                entity.HasIndex(e => e.RoleId)
                    .HasName("fkIdx_28");

                entity.HasIndex(e => e.UserId)
                    .HasName("fkIdx_31");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.XUserRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_28");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.XUserRole)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_31");
            });
        }
    }
}
