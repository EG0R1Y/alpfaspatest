﻿using System.Collections.Generic;

namespace IdentityServer.Models
{
	public static class RoleList
	{
		/// <summary>
		/// Роли: Analytic, Manager, Chief
		/// Values соответсвуют Id в БД, TODO загружать из БД
		/// </summary>
		public static Dictionary<string, int> Roles => new Dictionary<string, int>()
			{{"Analytic", 1}, {"Manager", 2}, {"Chief", 3}};
	}
}
