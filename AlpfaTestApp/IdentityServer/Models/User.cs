﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class User
    {
        public User()
        {
            XUserRole = new HashSet<XUserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Password { get; set; }

        public ICollection<XUserRole> XUserRole { get; set; }
    }
}
