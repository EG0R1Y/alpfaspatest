﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class XUserRole
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public bool Deleted { get; set; }

        public Role Role { get; set; }
        public User User { get; set; }
    }
}
