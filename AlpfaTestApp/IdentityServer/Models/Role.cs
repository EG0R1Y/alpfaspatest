﻿using System;
using System.Collections.Generic;

namespace IdentityServer.Models
{
    public partial class Role
    {
        public Role()
        {
            XUserRole = new HashSet<XUserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<XUserRole> XUserRole { get; set; }
    }
}
