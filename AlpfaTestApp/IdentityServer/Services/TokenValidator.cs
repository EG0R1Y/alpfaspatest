﻿using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Validation;

namespace IdentityServer.Services
{
	public class TokenValidator : ITokenValidator
	{
		public Task<TokenValidationResult> ValidateAccessTokenAsync(string token, string expectedScope = null)
		{
			throw new System.NotImplementedException();
		}

		public Task<TokenValidationResult> ValidateRefreshTokenAsync(string token, Client client = null)
		{
			throw new System.NotImplementedException();
		}

		public Task<TokenValidationResult> ValidateIdentityTokenAsync(string token, string clientId = null, bool validateLifetime = true)
		{
			throw new System.NotImplementedException();
		}
	}
}