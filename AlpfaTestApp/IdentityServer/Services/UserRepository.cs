﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Models;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Services
{
	public class UserRepository : IUserRepository
	{
		private TestUserContext _userContext;
		public UserRepository(TestUserContext userContext)
		{
			_userContext = userContext;
		}
		public async Task<User> FindAsync(string identityName)
		{
			return await _userContext.User.FromSql($"GetUserData 0, {UserDataOperation.GetUserByName}, {identityName}").FirstOrDefaultAsync();
		}

		public async Task<List<Claim>> GetUserClaims(object user)
		{
			var roles = await _userContext.Role.FromSql($"GetUserData {((User)user).Id}, {UserDataOperation.GetUserRoles}").ToListAsync();
			var claims = new List<Claim>();
			foreach (var role in roles)
			{
				claims.Add(new Claim(ClaimTypes.Role, role.Name));
			}

			return claims;
		}

		public async Task<object> FindAsync(long identityName)
		{
			return await _userContext.User.FromSql($"GetUserData 0, {UserDataOperation.GetUserById}").FirstOrDefaultAsync();
		}
	}
}