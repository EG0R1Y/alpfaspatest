﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Models;

namespace IdentityServer.Services
{
	public interface IUserRepository
	{
		Task<User> FindAsync(string identityName);
		Task<List<Claim>> GetUserClaims(object user);
		Task<object> FindAsync(long identityName);
	}

	
}