﻿namespace AlfaTestApp.BLL.Models
{
	public class ProductSummary
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public decimal Cost { get; set; }
		public int Count { get; set; }
	}
}
