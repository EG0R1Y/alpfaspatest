﻿namespace AlfaTestApp.BLL.Models
{
	public class RolesStatus
	{
		public bool isAnalytic { get; set; }
		public bool isManager { get; set; }
		public bool isChief { get; set; }
	}
}
