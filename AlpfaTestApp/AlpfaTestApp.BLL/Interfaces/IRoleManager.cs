﻿using System.Threading.Tasks;

namespace AlfaTestApp.BLL.Interfaces
{
	public interface IRoleManager
	{
		Task<bool> SetAnalyticRoleAsync(int userId, bool newValue);
		Task<bool> SetManagerRoleAsync(int userId, bool newValue);
		Task<bool> SetChiefRoleAsync(int userId, bool newValue);
		
	}
}