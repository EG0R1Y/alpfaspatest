﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlfaTestApp.BLL.Models;

namespace AlfaTestApp.BLL.Interfaces
{
	public interface IStorageDataAccess
	{
		Task<List<ProductSummary>> GetTopAsync();
		Task<List<ProductSummary>> GetActualAsync();
		Task<List<ProductSummary>> GetPopularAsync();
	}
}