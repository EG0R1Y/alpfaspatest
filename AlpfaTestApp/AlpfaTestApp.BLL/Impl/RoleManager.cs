﻿using System.Threading.Tasks;
using AlfaTestApp.BLL.Interfaces;
using IdentityServer.Models;
using Microsoft.EntityFrameworkCore;

namespace AlfaTestApp.BLL.Impl
{
	public class RoleManager : IRoleManager
	{
		private readonly TestUserContext _userContext;
		public RoleManager(TestUserContext context)
		{
			_userContext = context;
		}
		public async Task<bool> SetAnalyticRoleAsync(int userId, bool newValue)
		{
			var roleId = RoleList.Roles["Analytic"];
			return (await _userContext.ResultRoleChanged.FromSql($"UpdateUserRole {userId}, {roleId}, {newValue}").FirstAsync()).Success == 1;
		}

		public async Task<bool> SetManagerRoleAsync(int userId, bool newValue)
		{
			var roleId = RoleList.Roles["Manager"];
			return (await _userContext.ResultRoleChanged.FromSql($"UpdateUserRole {userId}, {roleId}, {newValue}").FirstAsync()).Success == 1;
		}

		public async Task<bool> SetChiefRoleAsync(int userId, bool newValue)
		{
			var roleId = RoleList.Roles["Chief"];
			return (await _userContext.ResultRoleChanged.FromSql($"UpdateUserRole {userId}, {roleId}, {newValue}").FirstAsync()).Success == 1;
		}

	
	}
}