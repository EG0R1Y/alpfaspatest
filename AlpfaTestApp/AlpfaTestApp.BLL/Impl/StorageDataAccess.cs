﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlfaTestApp.BLL.Interfaces;
using AlfaTestApp.DAL.Models;
using Microsoft.EntityFrameworkCore;
using ProductSummary = AlfaTestApp.BLL.Models.ProductSummary;

namespace AlfaTestApp.BLL.Impl
{
	public class StorageDataAccess : IStorageDataAccess
	{
		private readonly TestDataContext _dataContext;
		public StorageDataAccess(TestDataContext context)
		{
			_dataContext = context;
		}

		public async Task<List<ProductSummary>> GetTopAsync()
		{
			var productSummaries = await _dataContext.ProductSummaries.FromSql($"GetProductListByOperationId {CommandSelector.Top}").ToListAsync();
			return productSummaries.Select(p => new ProductSummary
				{Name = p.Name, Id = p.Id, Cost = p.Cost, Count = p.BuyCount}).ToList();
		}

		public async Task<List<ProductSummary>> GetActualAsync()
		{
			var  productSummaries = await _dataContext.ProductSummaries.FromSql($"GetProductListByOperationId {CommandSelector.Actual}").ToListAsync();
			return  productSummaries.Select(p => new ProductSummary
				{ Name = p.Name, Id = p.Id, Cost = p.Cost, Count = p.BuyCount }).ToList();
		}

		public async Task<List<ProductSummary>> GetPopularAsync()
		{
			var productSummaries = await _dataContext.ProductSummaries.FromSql($"GetProductListByOperationId {CommandSelector.Popular}").ToListAsync();
			return  productSummaries.Select(p => new ProductSummary
				{ Name = p.Name, Id = p.Id, Cost = p.Cost, Count = p.BuyCount }).ToList();
		}
	}
}