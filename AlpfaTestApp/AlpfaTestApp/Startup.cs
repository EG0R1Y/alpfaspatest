﻿using AlfaTestApp.BLL.Impl;
using AlfaTestApp.BLL.Interfaces;
using AlfaTestApp.DAL.Models;
using IdentityServer.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace AlfaTestApp.WebApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
			services.AddTransient<IStorageDataAccess, StorageDataAccess>();
			services.AddTransient<IRoleManager, RoleManager>();
			services.AddAuthorization( options =>
			{
				options.AddPolicy("RoleAccess", policy =>
					policy.RequireRole(RoleList.Roles.Keys));
			});
			services.AddAuthentication("Bearer")
				.AddJwtBearer("Bearer", options =>
				{
					options.Authority = "http://localhost:5015";
					options.RequireHttpsMetadata = false;
					
					options.Audience = "api1";
				});
			var corsUrl = Configuration.GetSection("SiteUrl").Value;
			services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
			{
				builder
					.AllowAnyMethod()
					.AllowAnyHeader()
					.WithOrigins(corsUrl)
					.AllowCredentials();
			}));
			services.AddDbContext<TestDataContext>(builder =>
				builder.UseSqlServer("Server=.\\SQLEXPRESS;Database=TestData;Trusted_Connection=True;"));
			services.AddDbContext<TestUserContext>(builder =>
				builder.UseSqlServer("Server=.\\SQLEXPRESS;Database=TestUser;Trusted_Connection=True;"));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseCors("CorsPolicy");
			app.UseAuthentication();
		//	app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
