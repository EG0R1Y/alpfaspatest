﻿using System.Threading.Tasks;
using AlfaTestApp.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AlfaTestApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
	    private readonly IStorageDataAccess _storageDataAccess;

	    public ProductController(IStorageDataAccess dataAccess)
	    {
		    _storageDataAccess = dataAccess;
	    }
	    [Authorize(Roles = "Manager")]
		[HttpGet("GetActual")]
	    public async Task<JsonResult> GetActual()
	    {
		    var products = await _storageDataAccess.GetActualAsync();
		    return new JsonResult(products);
	    }
	    [Authorize(Roles = "Chief")]
		[HttpGet("Top")]
	    public async Task<JsonResult> Top()
	    {
			var products = await _storageDataAccess.GetTopAsync();
			return new JsonResult(products);
		}
	    [Authorize(Roles = "Analytic")]
		[HttpGet("Popular")]
	    public async Task<JsonResult> Popular()
	    {
			var products = await _storageDataAccess.GetPopularAsync();
			return new JsonResult(products);
		}
	}
}