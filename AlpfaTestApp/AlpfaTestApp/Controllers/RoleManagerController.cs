﻿
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AlfaTestApp.BLL.Interfaces;
using AlfaTestApp.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AlfaTestApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
    public class RoleManagerController : ControllerBase
    {
	    private readonly IRoleManager _roleManager;
		/// <summary>
		/// По хорошему управление пользователями надо делать в
		/// Identity Server, но в текущем задании решил включить в WEB API
		/// </summary>
		/// <param name="roleManager"></param>
	    public RoleManagerController(IRoleManager roleManager)
	    {
		    _roleManager = roleManager;
	    }
	    [HttpPut("SetAnalyticRole")]
	    public async Task<JsonResult> SetAnalyticRole(RequestRoleValue request)
	    {
		    var claim = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier);
			var result = await _roleManager.SetAnalyticRoleAsync(int.Parse(claim.Value), request.NewValue);
		    return new JsonResult(result);
	    }
	    [HttpPut("SetManagerRole")]
	    public async Task<JsonResult> SetManagerRole(RequestRoleValue request)
	    {
		    var claim = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier);
			var result = await _roleManager.SetManagerRoleAsync(int.Parse(claim.Value), request.NewValue);
			return new JsonResult(result);
		}
	    [HttpPut("SetChiefRole")]
	    public async Task<JsonResult> SetChiefRole(RequestRoleValue request)
	    {
		    var claim = User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier);
			var result = await _roleManager.SetChiefRoleAsync(int.Parse(claim.Value), request.NewValue);
			return new JsonResult(result);
		}
	
	}
}