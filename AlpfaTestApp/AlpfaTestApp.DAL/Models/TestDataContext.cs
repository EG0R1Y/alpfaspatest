﻿using Microsoft.EntityFrameworkCore;

namespace AlfaTestApp.DAL.Models
{
    public partial class TestDataContext : DbContext
    {
        public TestDataContext()
        {
        }

        public TestDataContext(DbContextOptions<TestDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Buy> Buy { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Product> Product { get; set; }
		public virtual DbQuery<ProductSummary> ProductSummaries { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=TestData;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Buy>(entity =>
            {
                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Buy)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Buy_ClientId_Client_Id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Buy)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Buy_ProductId_Product_Id");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.Fio).IsRequired();
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Cost).HasColumnType("money");

                entity.Property(e => e.Name).IsRequired();
            });
        }
    }
}
