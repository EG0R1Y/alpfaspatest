﻿using System;

namespace AlfaTestApp.DAL.Models
{
    public partial class Buy
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ProductId { get; set; }
        public DateTime Date { get; set; }
        public int Count { get; set; }

        public Client Client { get; set; }
        public Product Product { get; set; }
    }
}
