﻿using System.Collections.Generic;

namespace AlfaTestApp.DAL.Models
{
    public partial class Product
    {
        public Product()
        {
            Buy = new HashSet<Buy>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }

        public ICollection<Buy> Buy { get; set; }
    }
}
