﻿using System.Collections.Generic;

namespace AlfaTestApp.DAL.Models
{
    public partial class Client
    {
        public Client()
        {
            Buy = new HashSet<Buy>();
        }

        public int Id { get; set; }
        public string Fio { get; set; }
        public int Rank { get; set; }

        public ICollection<Buy> Buy { get; set; }
    }
}
