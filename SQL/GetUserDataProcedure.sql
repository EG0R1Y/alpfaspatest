ALTER PROCEDURE GetUserData
	@id int,
	@Operation tinyint = 1,
	@name nvarchar(50) = ''
AS
	DECLARE @Op tinyint = @Operation;
	IF @Op = 1
	BEGIN
		SELECT * FROM [User] WHERE [User].Id = @id
	END
	IF @Op = 2
	BEGIN
		SELECT DISTINCT R.Id, R.Name FROM [User] U
		INNER JOIN [xUserRole] UR ON UR.UserId = U.Id
		INNER JOIN [Role] R ON R.Id = UR.RoleId
		WHERE U.Id = @id AND Deleted = 0
	END
	IF @Op = 0
	BEGIN
	PRINT @name
		SELECT * FROM [User] WHERE [User].Name = @name
	END

	