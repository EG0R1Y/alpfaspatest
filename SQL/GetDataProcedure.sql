ALTER PROCEDURE GetProductListByOperationId
@Operaton tinyint
AS
DECLARE @Op tinyint = @Operaton;
IF @Op = 1
	BEGIN
		
		SELECT P.Id, P.Name, P.Cost, COUNT(B.Id) AS BuyCount
		FROM Product P 
		INNER JOIN Buy B ON P.Id = B.ProductId 
		INNER JOIN Client C ON C.Id = B.ClientId
		WHERE C.Rank > 5
		GROUP BY  P.Id, P.Name, P.Cost
		HAVING COUNT(B.Id) > 3 
	END;
IF @Op = 2
	BEGIN
		
		SELECT P.Id, P.Name, P.Cost, COUNT(B.Id) AS BuyCount
		FROM Product P 
		INNER JOIN Buy B ON P.Id = B.ProductId
		INNER JOIN Client C ON C.Id = B.ClientId 
		GROUP BY  P.Id, P.Name, P.Cost
		HAVING COUNT(P.Id)>2
	END;
IF @Op = 3
	BEGIN
		
		SELECT TOP(5) P.Id, P.Name, P.Cost, COUNT(B.Id) AS BuyCount
		FROM Product P
		INNER JOIN Buy B ON P.Id = B.ProductId
		INNER JOIN Client C ON C.Id = B.ClientId 
		GROUP BY  P.Id, P.Name, P.Cost
		ORDER BY  SUM(P.Cost * B.Count) DESC
	END;
