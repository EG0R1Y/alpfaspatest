GO 
CREATE DATABASE [TestUser]
GO
USE [TestUser]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 06.02.2019 6:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 06.02.2019 6:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Password] [varbinary](32) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[xUserRole]    Script Date: 06.02.2019 6:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[xUserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_xUserRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([Id], [Name]) VALUES (1, N'Analytic')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (2, N'Manager')
INSERT [dbo].[Role] ([Id], [Name]) VALUES (3, N'Chief')
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Name], [Password]) VALUES (1, N'Ivanov', 0x03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4)
INSERT [dbo].[User] ([Id], [Name], [Password]) VALUES (2, N'Petrov', 0x03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4)
INSERT [dbo].[User] ([Id], [Name], [Password]) VALUES (3, N'Shepard', 0x03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4)
INSERT [dbo].[User] ([Id], [Name], [Password]) VALUES (4, N'Igor', 0x03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4)
INSERT [dbo].[User] ([Id], [Name], [Password]) VALUES (5, N'Semen', 0x03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4)
INSERT [dbo].[User] ([Id], [Name], [Password]) VALUES (6, N'Admin', 0x03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[xUserRole] ON 

INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (1, 1, 1, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (2, 1, 2, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (3, 1, 3, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (4, 2, 5, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (5, 3, 6, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (6, 2, 4, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (7, 1, 4, 1)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (8, 2, 1, 1)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (9, 3, 1, 0)
INSERT [dbo].[xUserRole] ([Id], [RoleId], [UserId], [Deleted]) VALUES (10, 3, 4, 0)
SET IDENTITY_INSERT [dbo].[xUserRole] OFF
/****** Object:  Index [fkIdx_28]    Script Date: 06.02.2019 6:21:15 ******/
CREATE NONCLUSTERED INDEX [fkIdx_28] ON [dbo].[xUserRole]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_31]    Script Date: 06.02.2019 6:21:15 ******/
CREATE NONCLUSTERED INDEX [fkIdx_31] ON [dbo].[xUserRole]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[xUserRole] ADD  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [dbo].[xUserRole]  WITH CHECK ADD  CONSTRAINT [FK_28] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[xUserRole] CHECK CONSTRAINT [FK_28]
GO
ALTER TABLE [dbo].[xUserRole]  WITH CHECK ADD  CONSTRAINT [FK_31] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[xUserRole] CHECK CONSTRAINT [FK_31]
GO
/****** Object:  StoredProcedure [dbo].[GetUserData]    Script Date: 06.02.2019 6:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserData]
	@id int,
	@Operation tinyint = 1,
	@name nvarchar(50) = ''
AS
	DECLARE @Op tinyint = @Operation;
	IF @Op = 1
	BEGIN
		SELECT * FROM [User] WHERE [User].Id = @id
	END
	IF @Op = 2
	BEGIN
		SELECT DISTINCT R.Id, R.Name FROM [User] U
		INNER JOIN [xUserRole] UR ON UR.UserId = U.Id
		INNER JOIN [Role] R ON R.Id = UR.RoleId
		WHERE U.Id = @id AND Deleted = 0
	END
	IF @Op = 0
	BEGIN
	PRINT @name
		SELECT * FROM [User] WHERE [User].Name = @name
	END

	
GO
/****** Object:  StoredProcedure [dbo].[UpdateUserRole]    Script Date: 06.02.2019 6:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUserRole]
@UserId int,
@RoleId int,
@NewValue bit
AS

	IF @NewValue = 0
	BEGIN
		UPDATE xUserRole SET Deleted = 1 WHERE UserId = @UserId AND RoleId = @RoleId
		SELECT 1 AS Success
		RETURN 
	END;
	IF @NewValue = 1
	BEGIN
		IF NOT EXISTS (SELECT * FROM xUserRole WHERE UserId = @UserId AND RoleId = @RoleId)
		BEGIN
			INSERT INTO xUserRole (UserId, RoleId, Deleted) 
			VALUES (@UserId, @RoleId, 0)
			SELECT 1 AS Success
			RETURN
		END
		ELSE
		BEGIN
			UPDATE xUserRole SET Deleted = 0 WHERE UserId = @UserId AND RoleId = @RoleId
			SELECT 1 AS Success
			RETURN
		END
	END;
SELECT 0 AS Success
RETURN
GO
