CREATE PROCEDURE UpdateUserRole
@UserId int,
@RoleId int,
@NewValue bit
AS

	IF @NewValue = 0
	BEGIN
		UPDATE xUserRole SET Deleted = 1 WHERE UserId = @UserId AND RoleId = @RoleId
		SELECT 1 AS Success
		RETURN 
	END;
	IF @NewValue = 1
	BEGIN
		IF NOT EXISTS (SELECT * FROM xUserRole WHERE UserId = @UserId AND RoleId = @RoleId)
		BEGIN
			INSERT INTO xUserRole (UserId, RoleId, Deleted) 
			VALUES (@UserId, @RoleId, 0)
			SELECT 1 AS Success
			RETURN
		END
		ELSE
		BEGIN
			UPDATE xUserRole SET Deleted = 0 WHERE UserId = @UserId AND RoleId = @RoleId
			SELECT 1 AS Success
			RETURN
		END
	END;
SELECT 0 AS Success
RETURN
